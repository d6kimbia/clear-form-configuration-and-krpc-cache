#! /bin/bash
# OPS-1103 - "Cache Clear script and cron"
# This is a bash file that will clear dev-platform form configurations.
# David Smith <d6@kimbia.com> - 20170815

# Initialize two variables, 'uri' is the page in Dev that triggers form configuration reloads. 'response' is the html data of that page, obtained via CURL.

uri=http://mars.kimbia.com/platform/control/reloadAllForms.html
response=$(curl -sL $uri) 

# Here we check to ensure that the response contains text telling us that the platform task completed successfully
if [[ "$response" =~ "OK - Platform Task Successful" ]]
then
        echo "[OK]  Platform Task Successful"
else
# if the page renders anything else but the above, there's a problem.
        echo "FAILED TO CLEAR FORM CONFIGURATIONS"
fi

# Todo
# . Sanity checks - Try again three times, and if all three attempts fail, give up and notify devops. 
# . create a notification bot integration for slack that alerts #ops that the platform task FAILED.
